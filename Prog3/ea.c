/*
Author: Fabian Claus
Matrikelnr: 20130004
Date: 15.10.19
*/

#include <stdio.h>
#include <math.h>
#include "ea.h"

void StartEaDialog() {
	struct InputData input = GetInput();

	int sum = input.first + input.second + input.third;
	int product = input.first * input.second * input.third;
	int min = input.first < input.second ? input.first : input.second;
	min = min < input.third ? min : input.third;
	int max = input.first > input.second ? input.first : input.second;
	max = max > input.third ? max : input.third;
	printf("Sum is %d\n",sum);
	printf("Average is %d\n", sum / 3);
	printf("Product is %d\n", product);
	printf("Smallest is %d\n",min);
	printf("Largest is %d\n",max);
}

struct InputData GetInput() {
	printf("\nInput three different integers: ");
	struct InputData input;
	scanf_s("%i %i %i", &input.first, &input.second, &input.third);
	return input;
}