/*
Author: Fabian Claus
Matrikelnr: 20130004
Date: 15.10.19
*/

#pragma once

struct InputData {
	int first, second, third;
};

void StartEaDialog();
struct InputData GetInput();