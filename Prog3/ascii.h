/*
Author: Fabian Claus
Matrikelnr: 20130004
Date: 15.10.19
*/

#pragma once
void PrintAsciiTable();
void PrintHeader();
void PrintRow(int startChar);
void PrintBottomLine();