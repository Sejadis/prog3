/*
Author: Fabian Claus
Matrikelnr: 20130004
Date: 15.10.19
*/

#include <stdio.h>
#include "ascii.h"

#define TOP_SEPERATOR 194
#define MIDDLE_SEPERATOR 197
#define LEFT_SEPERATOR 195
#define RIGHT_SEPERATOR 180
#define VERTICAL 179
#define HORIZONTAL 196
#define FILL_AMOUNT 16

void PrintAsciiTable() {
	int startChar = 32;
	PrintHeader();
	while (startChar < 255) {
		PrintRow(startChar);
		startChar += 4;
	}
	PrintBottomLine();
}

void PrintHeader() {
	//top row
	printf("%c", 218);
	for (int i = 0; i < 4; i++)
	{
		for (int i = 0; i < FILL_AMOUNT; i++)
		{
			printf("%c", HORIZONTAL);
		}
		printf("%c", i == 3 ? 191 : TOP_SEPERATOR);
	}
	printf("\n");

	//header title
	for (int i = 0; i < 4; i++)
	{
		printf("%c  dez  hex   z  ", VERTICAL);
	}
	printf("%c\n", VERTICAL);

	//bottom header row
	printf("%c", LEFT_SEPERATOR);
	for (int i = 0; i < 4; i++)
	{
		for (int i = 0; i < FILL_AMOUNT; i++)
		{
			printf("%c", HORIZONTAL);
		}
		printf("%c", i == 3 ? RIGHT_SEPERATOR : MIDDLE_SEPERATOR);
	}
	printf("\n");
}

void PrintRow(int startChar) {
	for (int i = 0; i < 4; i++)
	{
		printf("%c  %3d   %x   %c  ", VERTICAL, startChar, startChar, startChar);
		startChar++;
	}
	printf("%c\n", VERTICAL);
}

void PrintBottomLine() {
	printf("%c", 192);
	for (int i = 0; i < 4; i++)
	{
		for (int i = 0; i < FILL_AMOUNT; i++)
		{
			printf("%c", HORIZONTAL);
		}
		printf("%c", i == 3 ? 217 : 193);
	}
}